var fs = require('fs');
var cacheReaderFunction = require('./dataCache.js');
var configData = fs.readFileSync('../Config/configuration.json');
const axios = require('axios');
var request = require('sync-request');

// Nodejs encryption with CTR
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq';

const SKUAPIURL = JSON.parse(configData).SKUAPIURL;
const SKUAPI_CLIENTID = JSON.parse(configData).SKUTOKEN_CLIENTID;
const SKUAPI_CLIENTSECRET = decrypt(JSON.parse(configData).SKUTOKEN_SECRETID);
// client secret : ph(MH[=/w-Xpt2Yt

// Get token from CACHE
/*function GetTokenFromCache() {
    // Get token from cache
    var tokenInfo = cacheReaderFunction.GetCacheValue("appToken");
    if (typeof tokenInfo == "undefined" || tokenInfo == null) {
        GenerateToken();
        tokenInfo = cacheReaderFunction.GetCacheValue("appToken");
    }
    return tokenInfo;
}*/

// Get Token
function GetToken() {
    var response = "";
    var request = require('sync-request');
    var btoa = require('btoa');
    //var authInfo = "Basic " + btoa("warehouse:ph(MH[=/w-Xpt2Yt");
    var authInfo = "Basic " + btoa(SKUAPI_CLIENTID + ":" + SKUAPI_CLIENTSECRET);
    var headerInfo = {
        'Accept': 'text/json',
        'Authorization': authInfo,
        'Cache-Control': 'no-cache',
        'content-type': 'application/json'
    };

    var responce = request("POST", SKUAPIURL + "/v1/oauth2/token", {
        headers: headerInfo,
        body: 'grant_type=client_credentials',
        timeout: 180000
    });
    return responce.getBody('utf8');
}

//1. Get Token from cache
function GetTokenFromCache() {
    // Get token from cache
    var tokenInfo = cacheReaderFunction.GetCacheValue("appToken");

    if (typeof tokenInfo == "undefined" || tokenInfo == null) {
        var tokenData = GetTokenfromFile();
        tokenData.then(function (data) {
            tokenInfo = data;
            cacheReaderFunction.SetCacheValue("appToken", tokenInfo);
        }, function (err) {});
    } else {}
    return tokenInfo;
}

function GetTokenfromFile() {
    return new Promise(function (fulfill, reject) {
        fs.readFile('./SKUToken/token.txt', (err, data) => {
            if (err) {
                reject(err);
            } else {
                if (data != '') {
                    if (JSON.parse(data).access_token != '') {
                        cacheReaderFunction.SetCacheValue("appToken", data);
                        fulfill(data);
                    } else {
                        var tokenData = GenerateToken();
                        fulfill(tokenData);
                    }
                } else {
                    var tokenData = GenerateToken();
                    fulfill(tokenData);
                }
            }
        });
    });
}
// Generate Token
function GenerateToken() {
    var aToken = GetToken();
    cacheReaderFunction.SetCacheValue("appToken", aToken);
    fs.writeFile('./SKUToken/token.txt', aToken, function (error) {
        if (error) {} else {}
    });
    return aToken;
}

// Token Expiration Code
function TokenExpire(Exception) {
    if (Exception.statusCode == 401) {
        cacheReaderFunction.SetCacheValue("appToken", GetToken());
    }
}

// // Get the SKU Details
// function GetSKUDetails(sku, locale, effectivedate) {
//     try {
//         var url = SKUAPIURL + "/v1/enterprise_items" + "/" + sku + "?locale=" + locale + "&effectivedate=" + effectivedate;

//         var token = GetTokenFromCache();
//         var authInfo = "Bearer " + JSON.parse(token).access_token;

//         var headerInfo = {
//             'Accept': 'text/json',
//             'Authorization': authInfo,
//             'Cache-Control': 'no-cache',
//             'content-type': 'application/json'
//         };

//         var request = require('sync-request');
//         var responce = request("GET", url, {
//             headers: headerInfo,
//             body: null,
//             timeout: 180000
//         });
//         return responce.getBody('utf8');

//     } catch (err) {
//         throw err;
//     }
// }

function    GetSKUDetails(sku, locale, effectivedate, ipaddress, dateNow) {
        try {
            var urlInfo = SKUAPIURL + "/v1/enterprise_items" + "/" + sku + "?locale=" + locale + "&effectivedate=" + effectivedate;
            var token = GetTokenFromCache();
            var authInfo = "Bearer " + JSON.parse(token).access_token;
            let promise = new Promise((resolve, reject) => {
                axios
                    .get(urlInfo, {
                        headers: {
                            'Accept': 'text/json',
                            'Authorization': authInfo,
                            'Cache-Control': 'no-cache',
                            'content-type': 'application/json'
                        }
                    })
                    .then(res => {
                        let status = res.data.statusCode;
                        if (status == undefined) {
                            status = res.headers["crateapi-forwarded-status"];
                        }
                        let result = {
                            statusCode: res.status,
                            body: res.data
                        };                           
                        resolve(result);
                        // logData(ipaddress, "", sku, "GET SKU DETAILS", result.body.itemDetail.vendorID, dateNow, "SUCCESS").then((sucess) => {

                        //     })
                        //     .catch((error) => {
                        //         console.log(error);
                        //     });


                    })
                    .catch(err => {
                     
                        console.log(err);
                        let statusCode = err.toString().substring(err.toString().length - 3);
                        resolve({
                            statusCode: isNaN(statusCode) ? 417 : parseInt(statusCode)
                        });
                        // logData(ipaddress, "", sku, "GET SKU DETAILS", err, dateNow, "ERROR").then((sucess) => {

                        //     })
                        //     .catch((error) => {
                        //         console.log(error);
                        //     });
                    });
            });
            return promise;
            //return response.getBody('utf8');
        } catch (err) {
            
            console.log(err);
            return new Promise((resolve, reject) => {
                resolve({
                    statusCode: 417
                });
            });
            //throw err;
        }
    }


function getScannedSKU(srcFile) {
    return new Promise(function (fulfill, reject) {
        Quagga.decodeSingle({
            src: srcFile,
            numOfWorkers: 0, // Needs to be 0 when used within node
            locate: true,
            inputStream: {
                size: 800 // restrict input-size to be 800px in width (long-side)
            },
            decoder: {
                readers: ["code_128_reader"] // List of active readers
            },
        }, function (result) {
            if (result != undefined) {
                if (result.codeResult) {
                    fulfill(result.codeResult.code);
                } else {
                    reject("not detected");
                }
            } else {
                reject("Wrong Input");
            }

        });
    });
}

//GetDeviceInfo
function GetDeviceInfo(ipaddress) {
    var urlInfo;
    var headerInfo;
    try {
        // Header information
        var token = GetTokenFromCache();
        var authInfo = "Bearer " + JSON.parse(token).access_token;
        var headerInfo = {
            'Authorization': authInfo
        };
        urlInfo = SKUAPIURL + "/v1/ping?ipAddress=" + ipaddress;
        var request = require('sync-request');
        var response = request("GET", urlInfo, {
            headers: headerInfo,
            body: null
        });
        return response.getBody('utf8');
    } catch (err) {
        throw err;
    }
}




function encrypt(text) {
    var cipher = crypto.createCipher(algorithm, password)
    var crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
}

function decrypt(text) {
    var decipher = crypto.createDecipher(algorithm, password)
    var dec = decipher.update(text, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return dec;
}

function logData(ipaddress, repairnumber, sku, action, vendor, datenow, errorString) {
    let promise = new Promise((resolve, reject) => {
        var logStream = fs.createWriteStream('./Logs/log.csv', {
            'flags': 'a'
        });
        var location = "";
        var result = GetDeviceInfo(ipaddress);        
        if (result != null) {
            //Log with this JSON.parse(result).StoreID        
            location = JSON.parse(result).StoreID;
        }

        //Date, Action, Location, IPAddress, Repair Number, SKU, Vendor
        var logString = datenow + " , " + action + " , " + location + " , " + ipaddress.substring(7, ipaddress.length) + " , " + repairnumber + " , " + sku + " , " + vendor + ", " + errorString + "\n"
        logStream.write(logString);
        logStream.end();
    });
    return promise;
}

function logCountData(ipaddress, repairnumber, sku, action, vendor, datenow) {
    let promise = new Promise((resolve, reject) => {
        resolve({
            statusCode: 200
        });
        var logStream = fs.createWriteStream('./Logs/count.csv', {
            'flags': 'a'
        });
        var location = "";
        var result = GetDeviceInfo(ipaddress);
        if (result != null) {
            //Log with this JSON.parse(result).StoreID        
            location = JSON.parse(result).StoreID;
        }

        //Date, Action, Location, IPAddress, Repair Number, SKU, Vendor
        var logString = datenow + ", " + repairnumber + ", " + vendor + ", " + sku + ", " + location + ", " + action + "\n"
        logStream.write(logString);
        logStream.end();

    });
    return promise;
}


exports.logData = function (ipaddress, repairnumber, sku, action, vendor, datenow, errorString) {
    return logData(ipaddress, repairnumber, sku, action, vendor, datenow, errorString);
}

exports.logCountData = function (ipaddress, repairnumber, sku, action, vendor, datenow) {
    return logCountData(ipaddress, repairnumber, sku, action, vendor, datenow);
}

// Get Token
exports.GetToken = function () {
    return GetTokenFromCache();
}


exports.GetTokenFromCache = function () {
    //var tokenInfo = return GetTokenFromCache();
    //return tokenInfo;
    return GetTokenFromCache();
}

exports.GenerateToken - function () {
    return GenerateToken();
}

exports.getScannedSKU = function (srcFile) {
    return getScannedSKU(srcFile);
}

//Get SKU Details
exports.GetSKUDetails = function (sku, locale, effectivedate, ipaddress, dateNow) {
    // var result = "";
    // try {
    //     result = GetSKUDetails(sku, locale, effectivedate, ipaddress, dateNow);
    // } catch (Exception) {
    //     if (Exception.statusCode == 401) {
    //         GenerateToken();
    //         result = GetSKUDetails(sku, locale, effectivedate, ipaddress, dateNow);
    //     } else if (Exception.statusCode == 400) {
    //         return Exception;
    //     }
    // }

    return GetSKUDetails(sku, locale, effectivedate, ipaddress, dateNow);;
}


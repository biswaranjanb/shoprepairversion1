var express = require('express');
var app = express();
var bodyParser = require("body-parser"),
    formidable = require('formidable'),
    path = require('path'),
    async = require('async'),
        fs = require('fs'),
        Jimp = require("jimp"),
        Quagga = require('quagga').default;

var jsonParser = bodyParser.json();
var t = require('events').EventEmitter.prototype._maxListeners = 100;
var maxworkers = require('os').cpus().length;



var configData = fs.readFileSync('../Config/configuration.json');
var enableLogging = JSON.parse(configData).EnableLogging;
var nodeServerPORT = JSON.parse(configData).NodeServerPort;
var nodeServerURL = JSON.parse(configData).NodeServerURL;
var enableLoadLogin = JSON.parse(configData).EnableLoadLogin;

var skuData = require('./SKUToken/app.js');
var skuD = require('./SKUToken/app');
var gUpload = require('./gUpload/googleUpload.js');

var _dirName = '../TempImages/Upload/';
var _dirResize = '../TempImages/Resized/';
var UID;
var vendorName = '';
var skuName = '';
var vendorFullName = '';


app.use(jsonParser);
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser({
    limit: '50mb'
}));
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// Server Initialization
var server = app.listen(nodeServerPORT, function () {
    var host = server.address().address;
    var port = server.address().port;
});

//START-- Get SKU Details
app.post('/GetSKUDetails', function (req, res) {
    var ipaddress = (req.headers['x-forwarded-for'] || '').split(',').pop() ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        req.connection.socket.remoteAddress;
    var sku = req.body.sku.toString();
    var locale = req.body.locale.toString();
    var effectivedate = req.body.effectivedate.toString();
    //var result = skuData.GetSKUDetails(sku, locale, effectivedate);
    var nowdate = new Date();
    skuD.GetSKUDetails(sku, locale, effectivedate, ipaddress, nowdate)
        .then((result) => {

            vendorName = result.body.itemDetail.vendorID;
            vendorFullName = result.body.itemDetail.vendorName;
            skuName = sku;
            res.status(200).send(result);
        })
        .catch((error) => {
            console.log(error);
            res.status(417).send(error);
        });
});
//END-- Get SKU Details

//START--Upload to Google Drive
app.post('/uploadGoogleDrive/:sku/vendor/:vendorId', function (req, res) {
    //var ipaddress = req.header('x-forwarded-for') || req.connection.remoteAddress;
    var ipaddress = (req.headers['x-forwarded-for'] || '').split(',').pop() ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        req.connection.socket.remoteAddress;
    var form = new formidable.IncomingForm();
    var UID = gUpload.generateUUID();
    vendorName = req.params.vendorId;
    skuName = req.params.sku;
    if (!fs.existsSync(_dirName + '/' + UID)) {
        fs.mkdirSync(_dirName + '/' + UID);
        form.uploadDir = _dirName + '/' + UID;
    }
    if (!fs.existsSync(_dirResize + '/' + UID)) {
        fs.mkdirSync(_dirResize + '/' + UID);
    }
    form.parse(req).on('file', function (name, file) {
            var nowdate = new Date();
            if (enableLogging) {
                skuData.logData(ipaddress, file.name.substring(0, 9), skuName, "Image Upload", vendorName, nowdate, "SUCCESS").then((sucess) => {

                    })
                    .catch((error) => {
                        console.log(error);
                    });
            }
            var sourceFile = fs.createReadStream(file.path);

            var extension = file.name.substring(file.name.length - 4, file.name.length);
            skuData.logData(ipaddress, file.name.substring(0, 9) + "Extension is- " + extension, skuName, "Image Rename for multiple files same name", vendorName, nowdate, "SUCCESS").then((sucess) => {

                })
                .catch((error) => {
                    console.log(error);
                });
            var replacedName = file.name.replace(extension, "_" + gUpload.generateRandom() + extension)
            skuData.logData(ipaddress, file.name.substring(0, 9) + "Replaced Image name - " + replacedName, skuName, "Image Rename for multiple files same name", vendorName, nowdate, "SUCCESS").then((sucess) => {

                })
                .catch((error) => {
                    console.log(error);
                });
            var destFile = fs.createWriteStream(path.join(path.join(_dirResize, UID), replacedName));
            sourceFile.pipe(destFile);
            sourceFile.on('end', function () {
                skuData.logData(ipaddress, file.name.substring(0, 9), skuName, "Image Upload", vendorName, nowdate, "SUCCESS")
                    .then((sucess) => {

                    })
                    .catch((error) => {
                        console.log(error);
                    });
                skuData.logCountData(ipaddress, file.name, skuName, "IMAGE UPLOADED BY USER", vendorName, nowdate, "SUCCESS").then((sucess) => {})
                    .catch((error) => {
                        console.log(error);
                    });
                gUpload.imageResize(path.join(path.join(_dirResize, UID), replacedName), ipaddress, skuName, file.name.substring(0, 9), vendorName, nowdate);
            });
        })
        .on('end', function () {

            res.send('completed');
            var result = gUpload._googleDriveHandler(vendorName, vendorFullName, skuName, UID, ipaddress);
            result.then(function (data) {
                //res.send('completed');
            }, function (err) {
                //res.send('Error');
            });

        });
});
//END--Upload to Google Drive


//START-- Log Data
app.post('/logData/:sku/vendor/:vendorId', function (req, res) {
    var ipaddress = (req.headers['x-forwarded-for'] || '').split(',').pop() ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        req.connection.socket.remoteAddress
    var c = req.body.message.toString();

    //var ipaddress = req.header('x-forwarded-for') || req.connection.remoteAddress;
    var nowdate = new Date();
    vendorName = req.params.vendorId;
    skuName = req.params.sku;
    if (enableLoadLogin) {
        skuData.logData(ipaddress, "", skuName, req.body.message.toString(), vendorName, nowdate, "SUCCESS").then((sucess) => {})
            .catch((error) => {
                console.log(error);
            });
    }
});
//END-- Log Data
var fileCounter = 0;
var filesData= [];
var skuFilesData = [];
var repairFilesData = [];
var fileNameMain = null;
var file = null;
var base64Image = null;
var contentType = null;
var metadata = null;
var multipartRequestBody= null;
var fileNd= null;
var bUploaded = false;
var vendorID = null;
var numUploads = {};
numUploads.done = 0;
numUploads.total = 0;
var skuValid = false;
var validSKUID = null;
var invalidRepairNumber = true;
var bVendorNew = false;
var formData = new FormData();
var skuFormData = new FormData();
var repairFormData = new FormData();
var scannedRepair ;
var scannedSKU ='';
var repairNumberVal = '';
var skuNumberVal = '';
var currentServerURL ='' ;
var nodeServerURL ='';

var configFile = '../Config/configuration.json';
$(document).ready(function(){
    
  jQuery.support.cors = true;
  var skuHref = $('.skuScananchor').attr('href');
  var repairHref = $('.repairScananchor').attr('href');
  $('#progress').hide();

  function readConfigFile(){
    var jqxhr = $.getJSON( configFile, function() {
    })
    .done(function(configData) {
      currentServerURL = configData.CurrentServerURL ;
      nodeServerURL = configData.NodeServiceURL + ':' + configData.NodeServicePort ;
      logData("Application Load");
    })
  }
  readConfigFile();

  if(window.location.href.indexOf("scan") > -1) {
        scannedRepair = getParam('scan');
        var repair = scannedRepair.split("-")[0].split("=")[1];
        var sku = scannedRepair.split("-")[1];
        window.history.replaceState({}, document.title, window.location.href.split("?")[0]);
        $('.repairNumTxt').val(repair);
        $('.skunumText').val(sku);
  }
  $('.repairScananchor').click(function(){
      var skuNumID = $('.skunumText').val();
      skuNumberVal = $('.skunumText').val();
      var userAgent = navigator.userAgent || navigator.vendor || window.opera;
      if (/android/i.test(userAgent)) {
          location.href = "zxing://scan/?ret="+currentServerURL+"?scan={CODE}-" + skuNumID;
      }
      else {
        $('#myModalAlert').removeClass('hide');
        $('#myModalAlert').addClass('show');
        $('.errorMessage').text('Feature only available in android devices. Kindly enter the Repair # manually.');
        $('.closeSuccess').addClass('hide');
      }
  });
  $('.skuScananchor').click(function(){
      var repairNum = $('.repairNumTxt').val();
      repairNumberVal = $('.repairNumTxt').val();
      if((skuValid && filesData.length > 0))
     {
  				//Show a popup saying that Changes are made AND images will be removed.
  				$('#myModalConfirm').removeClass('hide');
  				$('#myModalConfirm').addClass('show');
  				$('.errorMessageConfirm').text('Do you want to remove the images for SKU '+ validSKUID +'?');
  				$('.confirmPopup').removeClass('hide');
  				$('.confirmPopup').addClass('show');
      }
      else
      {
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        if (/android/i.test(userAgent)) {
          location.href = "zxing://scan/?ret="+currentServerURL+"?scan="+repairNum+"-{CODE}";
        }
        else {
          $('#myModalAlert').removeClass('hide');
          $('#myModalAlert').addClass('show');
          $('.errorMessage').text('Feature only available in android devices. Kindly enter the SKU # manually.');
          $('.closeSuccess').addClass('hide');
        }
      }
  });


  $(document).on('click','.imageCaptureBtn', function	(){
    ValidateControls();
  });

  //START-- Input Field Events
  $(".repairNumTxt").on("input", function(e) {
  		maxLengthCheck($(this)[0]);
  });

  $(".skunumText").on("input", function(e) {
      /*if(invalidRepairNumber)
      {
        $('#myModalAlert').removeClass('hide');
        $('#myModalAlert').addClass('show');
        $('.errorMessage').text('Enter valid Repair #');
        $('.closeSuccess').addClass('hide');
        return false;
      }
      else
      {*/
          maxLengthCheck($(this)[0]);
      //}
  });

  $(".skunumText").on("change", function(e) {
  		// Check if files is present.
  		//If its present... Ask for confirmation about it.
  		if((skuValid && filesData.length > 0))
  		{
  				//Show a popup saying that Changes are made AND images will be removed.
  				$('#myModalConfirm').removeClass('hide');
  				$('#myModalConfirm').addClass('show');
  				$('.errorMessageConfirm').text('Do you want to remove the images for SKU '+ validSKUID +'?');
  				$('.confirmPopup').removeClass('hide');
  				$('.confirmPopup').addClass('show');
		}
  });

  //END-- Input Field Events

  // START--SKU Validate and get Vendor Details
  $(document).on('blur','.repairNumTxt' , function(){
        if($(this).val().length != 9)
        {
          $('#myModalAlert').removeClass('hide');
    		  $('#myModalAlert').addClass('show');
    		  $('.errorMessage').text('Enter valid Repair #');
          $('.closeSuccess').addClass('hide');
          invalidRepairNumber = true;
          $('.imageCaptureBtn').focus();
		      return false;
        }
        else{
            invalidRepairNumber = false;
        }
  });

  $(document).on('click touchstart', '.presskOK' , function(){
  	skuID = null;
  	filesData = [];
    $('#myModalConfirm').removeClass('show');
    $('#myModalConfirm').addClass('hide');
    $('.closeSuccess').addClass('hide');
    $('.innertableScanImage').html('');
    $('.skunumText').val('0');
  });

  $(document).on('click touchstart', '.pressCancel' , function(){
  	$(".skunumText").val(validSKUID);
  	$('#myModalConfirm').removeClass('show');
	$('#myModalConfirm').addClass('hide');
    $('.closeSuccess').addClass('hide');
  });
    // END--SKU Validate and get Vendor Details


     //START--Image remove option
    $(document).on('click', '.imgRemoveOption' , function(){
        var rowObject = $(this).closest('tr');
        var imgFileName = $(rowObject).find('.scannedImageTxt').attr('fileName');
        $(rowObject).remove();
        $('#myModalAlert').removeClass('hide');
        $('#myModalAlert').addClass('show');
        $('.errorMessage').text('SUCCESS ! Image removed ');
        $('.closeSuccess').addClass('hide');
        filesData = $.grep(filesData, function(e){ return e.fileName != imgFileName; });
        if(filesData.length == 0)
        {
            $('.btnUploadImage').attr('disabled','disabled');
            $('.skuScananchor').attr('href', skuHref);
            $('.repairScananchor').attr('href', repairHref);
        }
    });
    //END-Image remove option

    $(document).on('click touchstart','.close', function(){
        $('#myModalAlert').removeClass('show');
        $('#myModalAlert').addClass('hide');
    });

  //END--Common
  //START--Upload Google Drive
    $(document).on('click','.btnUploadImage', function	(){
        if(skuValid)
        {
            var vendorID = localStorage.getItem("vendorID");
            var vendorFullName = localStorage.getItem('vendorFullName');
            var sku = localStorage.getItem('sku');
            formFiles();
            $('.btnUploadImage').prop('disabled',true);
            $('#progress').show();
            $.ajax({
                type: "POST",
                url: nodeServerURL + '/uploadGoogleDrive/' + parseInt(localStorage.getItem("sku")) + '/vendor/' + vendorID,
                //url : nodeServerURL + '/uploadGoogleDrive',
                contentType: false,
                processData: false,
                data: formData,
                async: true,
                success: function (data) {
                    $('#progress').hide();
                    $('.btnUploadImage').prop('disabled',true);
                    if(data.toLowerCase() == 'completed')
                    {
                            $('#myModalAlert').removeClass('hide');
                            $('#myModalAlert').addClass('show');
                            $('.errorMessage').text('Images submitted for upload successfully');
                            $('.repairNumTxt').val('');
                            $('.skunumText').val('');
                    }
                    else{
                            $('#myModalAlert').removeClass('hide');
                            $('#myModalAlert').addClass('show');
                            $('.errorMessage').text('Image upload failed');
                            $('.repairNumTxt').val('');
                            $('.skunumText').val('');
                    }
                    $('.repairNumTxt').val(0);
                    $('.skunumText').val(0);
                    base64Image = null;
                    file = null;
                    base64Image = null;
                    contentType = null;
                    metadata = null;
                    multipartRequestBody= null;
                    $('.innertableScanImage').html('');
                    bUploaded = true;
                    filesData = [];
                    formData = new FormData();
                    $('.closeSuccess').addClass('hide');
                    $('.closeAll').addClass('show');
                    $('#lonChkBox').attr('checked', false);
                },
                error: function (xhr, textStatus, error) {

                },
                complete: function(){

                }
        });
        }
        else
        {
            $('#myModalAlert').removeClass('hide');
            $('#myModalAlert').addClass('show');
            $('.errorMessage').text('Enter valid SKU #');
            $('.closeSuccess').addClass('hide');
            return false;
        }

    });

    function formFiles(){
         $.each(filesData, function(index,value){
             filesData[index].name = filesData[index].fileName;
             formData.append('filesList[]', filesData[index],filesData[index].fileName) ;
         });
    }


    //END--Upload Google Drive
});

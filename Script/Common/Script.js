var skuError = false;
//START--Common
var getCurrentDateTime = function () {
    var currentdate = new Date();
    var datetime = currentdate.getFullYear() + "_" + (currentdate.getMonth() + 1) + "_" + currentdate.getDate() + "_" + currentdate.getHours() + "_" + currentdate.getMinutes() + "_" + currentdate.getSeconds() + "_" + currentdate.getMilliseconds();
    return datetime;
}

var getCurrentDate = function () {
    var currentdate = new Date();
    var currMon = (currentdate.getMonth() + 1).toString();
    var currDate = currentdate.getDate().toString();
    if (currMon.length < 2)
        currMon = "0" + currMon;

    if (currDate.length < 2)
        currDate = "0" + currDate;
    return currentdate.getFullYear() + "" + currMon + "" + currDate;
}
// Start -- SKU Validation
var validateSKU = function () {
    var skuID = $('.skunumText').val();
    if (skuID.length == 6) {
        var data = null;
        if ($('.lonChkBox').prop('checked') == true) {
            data = JSON.stringify({
                sku: skuID,
                locale: "nd-en-US",
                effectivedate: getCurrentDate()
            });
        } else {
            data = JSON.stringify({
                sku: skuID,
                locale: "cb-en-US",
                effectivedate: getCurrentDate()
            });

        }
        $.ajax({
            type: "POST",
            url: nodeServerURL + '/GetSKUDetails',
            contentType: 'application/json',
            data: data,
            async: false,
            success: function (data) {
                if (data.statusCode == 200) {
                    vendorID = data.body.itemDetail.vendorID;
                    vendorFullName = data.body.itemDetail.vendorName;
                    localStorage.setItem("vendorID", vendorID);
                    localStorage.setItem('vendorFullName', vendorFullName);
                    localStorage.setItem('sku', skuID);
                    if (vendorID != 0) {
                        skuValid = true;
                        validSKUID = skuID;
                    } else {
                        skuValid = false;
                        skuError = false;
                    }
                } else {
                    skuValid = false;
                    skuError = false;
                }
            },
            error: function (xhr, textStatus, error) {
                skuValid = false;
                skuError = true;
            }
        });
        return skuValid;
    } else {
        skuValid = false;
        return skuValid;
    }
    return skuValid;

}
// END--SKU Validation


// START--Validation
var ValidateControls = function () {
    var isValid = validateSKU();
    skuValid = isValid;
    if (isValid) {
        if (($(".repairNumTxt").val().length == 9) && ($(".skunumText").val().length != 0) && skuValid) {
            logData("ImageCapture button clicked");
            addFilesToUpload();
        } else {
            if (($(".repairNumTxt").val().length != 9) || ($(".skunumText").val().length == 0) || !skuValid) {
                if (($(".repairNumTxt").val().length != 9)) {
                    $('#myModalAlert').removeClass('hide');
                    $('#myModalAlert').addClass('show');
                    $('.errorMessage').text('Enter valid Repair #');
                    $('.closeSuccess').addClass('hide');
                    return false;
                }
                if ($(".skunumText").val() == 0 || !skuValid) {
                    if (skuError) {
                        $('#myModalAlert').removeClass('hide');
                        $('#myModalAlert').addClass('show');
                        $('.errorMessage').text('There is an error in Webservice. Please contact application support team.');
                        $('.closeSuccess').addClass('hide');
                        return false;
                    } else {
                        $('#myModalAlert').removeClass('hide');
                        $('#myModalAlert').addClass('show');
                        $('.errorMessage').text('Enter valid SKU #');
                        $('.closeSuccess').addClass('hide');
                        return false;
                    }

                }
            }
        }
    } else {
        if (skuError) {
            $('#myModalAlert').removeClass('hide');
            $('#myModalAlert').addClass('show');
            $('.errorMessage').text('There is an error in Webservice. Please contact application support team.');
            $('.closeSuccess').addClass('hide');
            return false;
        } else {
            $('#myModalAlert').removeClass('hide');
            $('#myModalAlert').addClass('show');
            $('.errorMessage').text('Enter valid SKU #');
            $('.closeSuccess').addClass('hide');
            return false;
        }
    }

}
// END--Validation

var logData = function (data) {
    var vendorID = localStorage.getItem("vendorID");
    if (vendorID == null) {
        vendorID = 0
    }
    var sku;
    if (localStorage.getItem("sku") == null) {
        sku = 0;
    } else {
        sku = parseInt(localStorage.getItem("sku"));
    }

    $.ajax({
        type: "POST",
        url: nodeServerURL + '/logData/' + sku + '/vendor/' + vendorID,
        contentType: 'application/json',
        async: true,
        data: JSON.stringify({
            message: data
        }),
        success: function (data) {},
        error: function (xhr, textStatus, error) {

        }
    });
}

//START--Input Fields Max Length Check
var maxLengthCheck = function (object) {
    if (object.value.length > object.maxLength)
        object.value = object.value.slice(0, object.maxLength)
    if (object.value === "" || object.value < 0)
        object.value = object.value.slice(0, object.value.length - 1);
}
//END--Input Fields Max Length Check



var scanBarCode = function (type, data) {
    $.ajax({
        type: "POST",
        url: nodeServerURL + '/ScanBarCode',
        contentType: false,
        processData: false,
        data: data,
        async: true,
        success: function (data) {
            if (type == 'SKU') {
                if (data == '007') {
                    $('#myModalAlert').removeClass('hide');
                    $('#myModalAlert').addClass('show');
                    $('.errorMessage').text('Invalid Barcode');
                    $('.closeSuccess').addClass('hide');
                } else {
                    $('.skunumText').val(data);
                }
            } else {
                if (data == '007') {
                    $('#myModalAlert').removeClass('hide');
                    $('#myModalAlert').addClass('show');
                    $('.errorMessage').text('Invalid Barcode');
                    $('.closeSuccess').addClass('hide');
                } else {
                    $('.repairNumTxt').val(data);
                }
            }
        },
        error: function (err) {
            if (type == 'SKU') {
                $('#myModalAlert').removeClass('hide');
                $('#myModalAlert').addClass('show');
                $('.errorMessage').text('Invalid Barcode');
                $('.closeSuccess').addClass('hide');
            } else {
                $('#myModalAlert').removeClass('hide');
                $('#myModalAlert').addClass('show');
                $('.errorMessage').text('Invalid Barcode');
                $('.closeSuccess').addClass('hide');
            }
        }
    });

}

// Start-- Capture Files after Camera Click
var addFilesToUpload = function () {
    var id = getUniqueIdentifier();
    // Log the image Capture Button.
    $(".UploadFileForm").append("<input id='" + id + "' type='file' added='false' class='" + id + " imageinput' added='false' name='" + id + "' style='display:none;' accept='image/x-png, image/gif, image/jpeg' multiple/>");

    document.getElementById(id).onchange = function (evt) {
        $('#' + id).attr('added', 'true');
        $('.btnUploadImage').attr('disabled', false);
        var tgt = evt.target || window.event.srcElement,
            files = tgt.files;
        if (FileReader && files && files.length) {
            $('.btnUploadImage').attr('disabled', false);
            var i = 0;
            $.each(files, function (index, value) {
                var name = value.name.split('.')[0];
                fileCounter = fileCounter + 1;
                //START-For Counters as File Suffix
                //var filename = $(".repairNumTxt").val() + '_' + $(".skunumText").val() + '_' + fileCounter ;
                //var file =$(".repairNumTxt").val() + '_' +$(".skunumText").val() + '_' + fileCounter +  '.' + value.name.split('.')[1];
                //END-For Counters as File Suffix
                var filename = $(".repairNumTxt").val() + '_' + getCurrentDateTime();
                var file = $(".repairNumTxt").val() + '_' + getCurrentDateTime() + '.' + value.name.split('.')[1];
                var hid = getUniqueIdentifier();
                $(".UploadFileForm").append("<input type='hidden' class='" + hid + "' name='" + name + "'/>");
                var imageData = "<tr class='InnerTableTR'><td class='InnerTable1st'><label class='scannedImageLbl'>" + file + "</label><input class='scannedImageTxt' value='" + filename + "' fileName='" + file + "' hidden /></td></td><td class='InnerTable3rd'><img  src='../Images/delete.png' class='imgRemoveOption'/></td></tr>'";
                $('.innertableScanImage').append(imageData);
                //$('.skuScananchor').attr("disabled","disabled");
                //$('.repairScananchor').attr("disabled","disabled");
                $('.skuScananchor').removeAttr('href');
                $('.repairScananchor').removeAttr('href');

                value.fileName = file;
                value.name = file;
                //  formData.append('uploadedFiles', value, file);
                filesData.push(value);
                i++;
            });
        }
    }
    $("." + id).click();
}
// End-- Capture Files after Camera Click

var getUniqueIdentifier = function () {
    return "uid_" + String(Math.floor(Math.random() * 1000000000)).replace(".", "");
}



function getParam(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return "";
    else {
        return results[0];
    }
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};